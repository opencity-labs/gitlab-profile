# Opencity Labs

Owner of the open source product Opencity Italia, a digital transformation platform for Public Administrations.

For more info check our company website [opencitylabs.it](https://opencitylabs.it) or our product page [opencityitalia.it](https://opencityitalia.it)

